import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../provider/passwords_provider.dart';
import '../provider/folder.dart';

class FolderListItem extends StatelessWidget {
  final Folder? _folder;
  final Function? onTap;
  final Function? onLongPress;
  final IconData iconData;
  final int? level;
  final Widget? trailing;

  const FolderListItem(this._folder,
      {this.onTap,
      this.onLongPress,
      this.iconData = Icons.folder_rounded,
      this.level = 0,
      this.trailing});

  @override
  Widget build(BuildContext context) {
    final isLocal = Provider.of<PasswordsProvider>(
      context,
      listen: false,
    ).isLocal;
    return Column(
      children: [
        Row(
          children: [
            SizedBox(
              width: 15.0 * level!,
            ),
            Expanded(
              child: ListTile(
                leading: Icon(
                  iconData,
                  size: 40,
                  color: Theme.of(context).colorScheme.secondary,
                ),
                title: Text(_folder!.label),
                onTap: onTap as void Function()?,
                onLongPress: isLocal ? null : onLongPress as void Function()?,
                trailing: trailing,
              ),
            ),
          ],
        ),
        Divider(
          color: Theme.of(context).colorScheme.secondary.withAlpha(50),
        ),
      ],
    );
  }
}
